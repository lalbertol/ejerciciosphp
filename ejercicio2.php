<!DOCTYPE html>
<html>
<head>
    <title>Calcular IVA</title>
</head>
<body>
<h1>CALCULAR IVA</h1>
<br>
<p>
 <?php
       $precioUnidad = 5;
        $cantidad = 10;
        $iva = 1.21;

        $precioSinIva = $cantidad * $precioUnidad;

        $precioConIva = ($cantidad * $precioUnidad) * $iva;

        echo "El precio con IVA de unidad $precioUnidad con la cantidad de $cantidad es es $precioConIva" ;

        echo "<br>";

        echo "El precio sin IVA de unidad $precioUnidad con la cantidad de $cantidad es es $precioSinIva" ;

    ?>
</p>

</body>
</html>
