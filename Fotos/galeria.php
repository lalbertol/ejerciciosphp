<!DOCTYPE html>
<html>
<head>
    <title>Galeria</title>
</head>
<body>
    <h1>Galeria de Imagenes</h1>
    <?php foreach ($array as $key => $imagen): ?>

        <img width="150" height="150" src=<?php echo "uploads/$imagen" ?>> <!-- adjuntamos la imagen -->
        <li><?php echo $imagen ?></li>
        <a>
            <!-- creamos los dos botones de mostrar y borrar como un formulario y le ponemos el valor la imagen que le toca en el foreach -->
            <form method="post" action="?method=mostrar">
                <input type="hidden" name="mostrar" value="<?php echo "uploads/$imagen" ?>">
                <input type="submit" value="Ver">

            </form>
        </a>

        <a>
            <form method="post" action="?method=borrar">
                <input type="hidden" name="file" value="<?php echo "uploads/$imagen" ?>">
                <input type="submit" value="Borrar" target="_self"><br><br>
            </form>
        </a>

    <?php endforeach ?>


</body>
</html>

